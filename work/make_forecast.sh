#!/bin/bash

mkdir ../prod/$1
for file in `ls processed/gfs/$1`
do
	cat processed/gfs/$1/$file processed/omw/$1/$file > ../prod/$1/$file
done
