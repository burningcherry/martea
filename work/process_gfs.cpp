#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cmath>

std::string get_padded_str(const int x) {
	if (x == 0)
		return "000";
	std::string result = "";
	for (int i = 0; i < 2 - log10(x); ++i)
		result += '0';
	return result + std::to_string(x);
}

int main(int argc, char const *argv[])
{
	using namespace std;

	string hour = string(argv[1]).substr(8,2);

	// for (int j = 0; j <= 180; j += 3) {
	// 	printf("%d\n", j);
	// 	system((string("wgrib2 raw/gfs/") + argv[1] + "/gfs.t" + hour + "z.pgrb2.0p25.f" + get_padded_str(j) + " -set_grib_type same -new_grid_winds grid -new_grid -180:1440:0.25 -77.5:621:0.25 processed/gfs/" + argv[1] + '/' + get_padded_str(j) + " >/dev/null").c_str());
	// }

	vector<float> map1 = vector<float>(2*1440*721),
				  map2 = vector<float>(2*1440*721),
				  map3 = vector<float>(2*1440*721),
				  map4 = vector<float>(2*1440*721);

	// system("rm ieee ieee2 ieee3 ieee4");
	// system("mkfifo ieee ieee2 ieee3 ieee4");
	system((string("mkdir processed/gfs/") + argv[1]).c_str());

	for (int j = 0; j < 120; j += 3) {
		printf("%d\n", j);
		system((string("cp raw/gfs/") + argv[1] + "/gfs.t" + argv[1][8] + argv[1][9] + "z.pgrb2.0p25.f" + get_padded_str( j ) + " processed/gfs/" + argv[1] + '/' + get_padded_str( j )).c_str()); 
		system((string("cp raw/gfs/") + argv[1] + "/gfs.t" + argv[1][8] + argv[1][9] + "z.pgrb2.0p25.f" + get_padded_str(j+3) + " processed/gfs/" + argv[1] + '/' + get_padded_str(j+3)).c_str()); 
		system((string("wgrib2 processed/gfs/") + argv[1] + '/' + get_padded_str( j ) + " -no_header -little_endian -ieee ieee >/dev/null").c_str());
		system((string("wgrib2 processed/gfs/") + argv[1] + '/' + get_padded_str(j+3) + " -no_header -little_endian -ieee ieee2 >/dev/null").c_str());
		// system((string("wgrib2 raw/gfs/") + argv[1] + "/gfs.t" + argv[1][8] + argv[1][9] + "z.pgrb2.0p25.f" + get_padded_str(j+3) + " -no_header -little_endian -ieee ieee2 >/dev/null &").c_str());
		
		// printf("%d\n", j);
		ifstream in1("ieee"), in2("ieee2");
		ofstream out1("ieee3"), out2("ieee4");

		in1.read((char*)&map1[0], 4*2*1440*721);
		in2.read((char*)&map2[0], 4*2*1440*721);

		for (int i = 0; i < 2*1440*721; ++i) {
			map3[i] = (map1[i]*2 + map2[i])/3.0;
			map4[i] = (map1[i] + map2[i]*2)/3.0;
		}

		out1.write((char*)&map3[0], 4*2*1440*721);
		out2.write((char*)&map4[0], 4*2*1440*721);

		system((string("wgrib2 raw/gfs/") + argv[1] + "/gfs.t" + argv[1][8] + argv[1][9] + "z.pgrb2.0p25.f" + get_padded_str( j ) + " -no_header -import_ieee ieee3 -little_endian -set_ftime \"" + to_string(j+1) + " hour fcst\" -set_grib_type same -grib_out processed/gfs/" + argv[1] + '/' + get_padded_str(j+1) + " >/dev/null").c_str());
		system((string("wgrib2 raw/gfs/") + argv[1] + "/gfs.t" + argv[1][8] + argv[1][9] + "z.pgrb2.0p25.f" + get_padded_str( j ) + " -no_header -import_ieee ieee4 -little_endian -set_ftime \"" + to_string(j+2) + " hour fcst\" -set_grib_type same -grib_out processed/gfs/" + argv[1] + '/' + get_padded_str(j+2) + " >/dev/null").c_str());

		in1.close();
		in2.close();
		out1.close();
		out2.close();
	system("rm ieee ieee2 ieee3 ieee4");

	}

	for (int j = 123; j <= 180; j += 3)
		system((string("cp raw/gfs/") + argv[1] + "/gfs.t" + argv[1][8] + argv[1][9] + "z.pgrb2.0p25.f" + get_padded_str( j ) + " processed/gfs/" + argv[1] + '/' + get_padded_str( j )).c_str());

	return 0;
}