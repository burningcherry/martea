#!/bin/bash
regex="multi_1.glo_30m.t(00|06|12|18)z.f([0-9]{3}).grib2"
mkdir "processed/omw/$1"
for file in `ls raw/omw/$1`
do
	# echo $file
	if [[ $file =~ $regex ]]
	then
		hour="${BASH_REMATCH[2]}"
		# echo raw/omw/$1/$file processed/omw/$1/$hour
#why not bicubic? need to check
		wgrib2 "raw/omw/$1/$file" \
		-set_grib_type same \
		-new_grid_winds grid \
		-new_grid_interpolation bilinear \
		-new_grid latlon -180:1440:0.25 -77.5:621:0.25 \
		"processed/omw/$1/$hour"
	fi
done
