#!/usr/bin/perl

sub getRoundedDateStr {
    my ($hourinterval) = @_;
    my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime(time() - (6 * 3600));
    # my $actual_time = time() - (6 * 3600);
    my $dayofm = sprintf("%02d", $mday);
    my $newyear = $year + 1900;

    my $hours = sprintf("%02d", $hour);
    my $month = $mon += 1;
    $month = sprintf("%02d", $month);
    my $result = '';
    #print $newyear.$month.$dayofm.$hours."\n";
    if( $hourinterval ne 0) {
        my $hh = int($hour / $hourinterval) * $hourinterval;
        $hh = sprintf("%02d", $hh);
        $result =  $newyear.$month.$dayofm.$hh;
    } else {
        $result = $newyear.$month.$dayofm;
    }

    return $result;
}

sub is_folder_empty {
    my $dirname = shift;
    opendir(my $dh, $dirname) or die "Not a directory";
    return scalar(grep { $_ ne "." && $_ ne ".." } readdir($dh)) == 0;
}

my $rounded_date = getRoundedDateStr(6);
my $hourr = substr($rounded_date, 8 ,2);
my $dst_cat = 'raw/omw/'.$rounded_date;
my $update_available = 0;

my $noaa_url = 'http://nomads.ncep.noaa.gov/pub/data/nccf/com/wave/prod/multi_1.'.substr($rounded_date, 0, 8);
my $file_pattern = 'multi_1.glo_30m.t'.$hourr.'z.f';

if ( -e $dst_cat ){
    print "OMW: Katalog istnieje\n";
} elsif (scalar(`lynx -dump -listonly  $noaa_url  | grep $file_pattern`) < 280) {
    print "OMW: Dane jeszcze nie wrzucone\n";
} else {
    print "OMW: Katalog nie istnieje. Tworze... $dst_cat\n";
    mkdir($dst_cat, 0755);

    system("./get_omw.pl data ".$rounded_date." 0 120 1 WIND:WDIR surface ".$dst_cat);
    system("./get_omw.pl data ".$rounded_date." 123 180 3 WIND:WDIR surface ".$dst_cat);

    system("./process_omw.sh ".$rounded_date);

    $update_available = 1;
}

$dst_cat = 'raw/gfs/'.$rounded_date;
$noaa_url = 'http://nomads.ncep.noaa.gov/pub/data/nccf/com/gfs/prod/gfs.'.$rounded_date;
$file_pattern = 'gfs.t'.$hourr.'z.pgrb2.0p25.f';

if ( -e $dst_cat ){
    print "GFS: Katalog istnieje\n";
} elsif (scalar(`lynx -dump -listonly  $noaa_url  | grep $file_pattern`) < 120) {
    print "GFS: Dane jeszcze nie wrzucone\n";
} else {
    print "GFS: Katalog nie istnieje. Tworze... $dst_cat\n";
    mkdir($dst_cat, 0755);

    system("./get_gfs.pl data ".$rounded_date." 0 180 3 UGRD:VGRD \"10 m above ground\" ".$dst_cat);

    system("./process_gfs ".$rounded_date);
    
    $update_available = 1;
}

if ($update_available) { # doszła nowa prognoza
	if (-e 'raw/omw/'.$rounded_date && -e 'raw/gfs/'.$rounded_date) { # i są już obie
        system("./make_forecast.sh ".$rounded_date);
        # wyczyść /raw, /processed i /prod ze staroci
        print "OK";
    }
}